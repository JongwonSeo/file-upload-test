const multer = require('multer');

module.exports.avatar = {
  // all the file uploading script will be defined here

  storage() {
    const storage = multer.diskStorage({
      destination(req, file, cb) {
        cb(null, 'public/uploads/');
      },
      filename(req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}`);
      },
    });

    return storage;
  },
  allowedImage(req, file, cb) {
    // Accept images only
    if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
      req.fileValidationError = 'Only image files are allowed!';
      return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
    return null;
  },
};
