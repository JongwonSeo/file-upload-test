const multer = require('multer');
const multipleFileUpload = require('../config/multiple-file-upload');

module.exports = {
  uploadForm(req, res) {
    res.render('upload-form');
  },
  uploadFile(req, res) {
    const upload = multer({
      storage: multipleFileUpload.avatar.storage(),
      allowedImage: multipleFileUpload.avatar.allowedImage,
    }).single('avatar');
    upload(req, res, function (err) {
      if (err instanceof multer.MulterError) {
        res.send(err);
      } else if (err) {
        res.send(err);
      } else {
        res.render('multiple-upload-form');
      }
    });
    console.log('It was uploaded successfully');
  },
};
