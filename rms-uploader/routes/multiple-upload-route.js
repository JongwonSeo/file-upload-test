const express = require('express');

const router = express.Router();
const multipleUploadController = require('../controllers/multiple-upload-controller');

router.get('/upload', multipleUploadController.uploadForm);

router.post('/upload', multipleUploadController.uploadFile);

module.exports = router;
