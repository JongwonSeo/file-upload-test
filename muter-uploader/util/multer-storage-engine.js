const fs = require('fs');

const db = require('./db');

function getDestination(req, file, cb) {
  cb(null, `upload-storage/${file.originalname}`);
}

function MyCustomStorage(opts) {
  this.getDestination = opts.destination || getDestination;
}

MyCustomStorage.prototype._handleFile = function _handleFile(req, file, cb) {
  // eslint-disable-next-line consistent-return
  console.log('_handleFile');
  this.getDestination(req, file, (err, filePath) => {
    if (err) return cb(err);

    console.log('############');
    console.log(req.headers['x-file-id']);
    const fileId = req.headers['x-file-id'];
    const startByte = +req.headers['x-start-byte'] || 0;

    if (!db[fileId]) db[fileId] = {};
    const upload = db[fileId];
    console.log(startByte);
    console.log(upload.bytesReceived);

    let fileStream;

    if (!startByte) {
      upload.bytesReceived = 0;
      fileStream = fs.createWriteStream(filePath, {
        flags: 'w',
      });
      console.log(`New file created: ${filePath}`);
    } else {
      // we can check on-disk file size as well to be sure
      if (upload.bytesReceived !== startByte) {
        res.writeHead(400, 'Wrong start byte');
        res.end(upload.bytesReceived);
        return null;
      }
      // append to existing file
      fileStream = fs.createWriteStream(filePath, {
        flags: 'a',
      });
      console.log(`File reopened: ${filePath}`);
    }
    req.on('data', function (data) {
      upload.bytesReceived += data.length;
    });

    file.stream.pipe(fileStream);

    fileStream.on('close', function () {
      console.log('close');
    });

    // in case of I/O error - finish the request
    // fileStream.on('error', cb);
    fileStream.on('error', function (err) {
      console.log('fileStream error');
    });

    fileStream.on('finish', function () {
      console.log('finish');
      cb(null, {
        filePath,
        size: fileStream.bytesWritten,
      });
    });
  });
};

MyCustomStorage.prototype._removeFile = function _removeFile(req, file, cb) {
  // fs.unlink(file.path, cb);
};

module.exports = function (opts) {
  return new MyCustomStorage(opts);
};
