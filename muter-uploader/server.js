const express = require('express');
const path = require('path');

const app = express();
const port = 4000;

const indexRouter = require('./routes/index');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));


app.use('/', indexRouter);

app.listen(port, () => {
  console.log(`listening at http://localhost:${port}`);
});

module.exports = app;
