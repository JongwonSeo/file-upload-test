const express = require('express');
const multer = require('multer');
const db = require('../util/db');
const storageEngine = require('../util/multer-storage-engine');

const router = express.Router();

const upload = multer({
  storage: storageEngine({
    destination(req, file, cb) {
      cb(null, `upload-storage/${file.originalname}`);
    },
  }),
});

router.post('/multi', upload.array('formFiledNameMulti'), uploadMulti);
router.get('/status', onStatus);
router.get('/', uploadPage);

function uploadPage(req, res) {
  res.render('upload');
}

function uploadMulti(req, res, next) {
  console.log('#################!!!');
  if (next) {
    console.log('next');
    return res.end('Next');
  }
  return res.end('File is uploaded');
}

function onStatus(req, res) {
  const fileId = req.headers['x-file-id'];
  const uploadFile = db[fileId];
  console.log("fileId");
  console.log(fileId);
  console.log('onStatus fileId:', fileId, ' upload:', uploadFile);
  if (!uploadFile) {
    res.end('0');
  } else {
    res.end(String(uploadFile.bytesReceived));
  }
}

module.exports = router;
