module.exports = {
  bracketSpacing: false,
  singleQuote: true,
  // trailingComma: 'all',
  printWidth: 140,
  tabWidth: 2,
  useTabs: false,
  angleBracketSameLine: true,
  overrides: [
    {
      files: ['**/*.css', '**/*.scss', '**/*.html'],
      options: {
        singleQuote: false,
      },
    },
  ],
};
