const express = require('express');

const app = express();
const fm = require('formidable');
const fs = require('fs');
const http = require('http');
const path = require('path');

// local uploads object
const uploads = Object.create(null);

app.set('port', 3000);
app.use(express.static(path.join(__dirname, 'public')));

app.post('/fileimport', function (req, res) {
  const nfm = new fm.IncomingForm({
    uploadDir: `${__dirname}/uploadfiles`,
    keepExtensions: true,
  });
  nfm.parse(req, function (error, fields, files) {
    console.log(fields);
    console.log(files);
  });
  nfm.on('field', function (field, value) {
    nfm[field] = value;
    return nfm[field];
  });
  nfm.on('fileBegin', function (name, file) {
    // jwseo
    const fileId = file.id;
    const filedes = {
      filename: file.name,
      serverPath: file.path,
      originalSize: nfm.filesize,
      originalLMD: nfm.lastModifiedDate,
      finish: false,
    };
    if (!uploads[fileId]) uploads[fileId] = {};
    uploads[fileId] = {...filedes};
    // return app.get('collection').insert(
    //   filedes,
    //   {
    //     w: 1,
    //   },
    //   function (err, result) {
    //     nfm.fileId = result[0]._id;
    //     return nfm.fileId;
    //   }
    // );
  });
  return nfm.on('file', function (name, file) {
    return app.get('collection').update(
      {
        _id: nfm.fileId,
      },
      {
        $set: {
          finish: true,
        },
      },
      {
        w: 1,
      },
      function (err, result) {
        return res.send(nfm.fileId.toString());
      }
    );
  });
});

app.post('/fileimport/:fid', function (req, res) {
  const nfm = new fm.IncomingForm();
  nfm.onPart = function (part) {
    return part.on('data', function (buffer) {
      return fs.writeSync(nfm.fd, buffer, 0, buffer.length, null, 'Binary');
    });
  };
  return app.get('collection').findOne(
    {
      _id: new ObjectID(req.params.fid),
    },
    function (err, item) {
      if (item) {
        return fs.open(item.serverPath, 'a', function (err, fd) {
          nfm.fd = fd;
          return nfm.parse(req, function (error, fields, files) {
            fs.closeSync(fd);
            return fs.stat(item.serverPath, function (err, stat) {
              if (stat.size.toString() === item.originalSize.toString()) {
                return app.get('collection').update(
                  {
                    _id: item._id,
                  },
                  {
                    $set: {
                      finish: true,
                    },
                  },
                  {
                    w: 1,
                  },
                  function (err, result) {
                    return res.send(item._id.toString());
                  }
                );
              }
              return res.end();
            });
          });
        });
      }
      return res.end();
    }
  );
});

app.get('/fileimport', function (req, res) {
  return app.get('collection').findOne(
    {
      filename: req.query.filename,
      originalSize: req.query.filesize,
      originalLMD: req.query.lastModifiedDate,
      finish: false,
    },
    function (err, item) {
      if (item) {
        return fs.stat(item.serverPath, function (err, stat) {
          return res.send({
            exist: true,
            id: item._id,
            currentSize: stat.size,
          });
        });
      }
      return res.send({
        exist: false,
      });
    }
  );
});

app.get('/fileimport/:fid', function (req, res) {
  return app.get('collection').findOne(
    {
      _id: new ObjectID(req.params.fid),
    },
    function (err, item) {
      if (item) {
        return res.download(item.serverPath, item.filename);
      }
      return res.end();
    }
  );
});

http.createServer(app).listen(app.get('port'), function () {
  return console.log(`Express server listening on port ${app.get('port')}`);
});
