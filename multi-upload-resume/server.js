const express = require('express');
const cors = require('cors');
const path = require('path');
const spdy = require('spdy');
const fs = require('fs');

const options = {
  key: fs.readFileSync(path.join(__dirname, 'ssl/local.key')),
  cert: fs.readFileSync(path.join(__dirname, 'ssl/local.crt')),
  // key: fs.readFileSync('ssl/server.key'),
  // cert: fs.readFileSync('ssl/server.crt'),
  passphrase: '개인키를 만들때 생성한 비밀번호',
};

const app = express();

app.set('port', process.env.PORT || 8000);
app.use('/public', express.static(`${__dirname}/public`));
app.use(cors());

app.set('views', `${__dirname}/public/views`);
app.engine('html', require('ejs').renderFile);

app.set('view engine', 'html');

let uploads = Object.create(null);

function onUpload(req, res) {
  const fileId = req.headers['x-file-id'];
  const startByte = +req.headers['x-start-byte'];

  if (!fileId) {
    res.writeHead(400, 'No file id');
    res.end();
  }

  const filePath = path.join('./uploadFile', fileId);
  console.log('onUpload fileId: ', fileId);
  if (!uploads[fileId]) uploads[fileId] = {};
  const upload = uploads[fileId];

  console.log(`bytesReceived:${upload.bytesReceived} startByte:${startByte}`);

  let fileStream;

  if (!startByte) {
    upload.bytesReceived = 0;
    fileStream = fs.createWriteStream(filePath, {
      flags: 'w',
    });
    console.log(`New file created: ${filePath}`);
  } else {
    if (upload.bytesReceived !== startByte) {
      res.writeHead(400, 'Wrong start byte');
      res.end(upload.bytesReceived);
      return;
    }
    fileStream = fs.createWriteStream(filePath, {
      flags: 'a',
    });
    console.log(`File reopened: ${filePath}`);
  }

  req.on('data', function (data) {
    upload.bytesReceived += data.length;
  });

  req.pipe(fileStream);

  fileStream.on('close', function () {
    if (upload.bytesReceived === req.headers['x-file-size']) {
      console.log('Upload finished');
      delete uploads[fileId];

      res.end(`Success ${upload.bytesReceived}`);
    } else {
      console.log(`File unfinished, stopped at ${upload.bytesReceived}`);
      res.end();
    }
  });

  fileStream.on('error', function (err) {
    console.log('fileStream error');
    res.writeHead(500, 'File error');
    res.end();
  });
}

function onStatus(req, res) {
  const fileId = req.headers['x-file-id'];
  const upload = uploads[fileId];
  console.log('onStatus fileId:', fileId, ' upload:', upload);
  if (!upload) {
    res.end('0');
  } else {
    res.end(String(upload.bytesReceived));
  }
}

app.get('/status', function (req, res) {
  onStatus(req, res);
});

app.get('/reset', function (req, res) {
  uploads = Object.create(null);
  res.end('database reset');
});

app.get('/', function (req, res) {
  res.render('index.html');
});

app.post('/upload', function (req, res) {
  onUpload(req, res);
});

spdy.createServer(options, app).listen(app.get('port'), () => {
  console.log(`App started listening on PORT ${app.get('port')}`);
});

// app.listen(app.get('port'), () => {
//   console.log(`Example app listening at http://localhost:${app.get('port')}`);
// });
