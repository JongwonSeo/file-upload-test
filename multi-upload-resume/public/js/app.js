(function (root) {
  const formUploadEl = document.querySelector('.form-upload');
  const inputFilesEl = document.querySelector('.input-files');
  const listUploadEl = document.querySelector('.list-upload');
  const buttonUpload = document.querySelector('.button-upload');
  const buttonStop = document.querySelector('.button-stop');
  const buttonReset = document.querySelector('.button-reset');

  let isStop = false;
  let uploader;

  formUploadEl.addEventListener('submit', uploadFile);
  inputFilesEl.addEventListener('change', changeFile, false);

  buttonStop.addEventListener('click', uploadStop, false);
  buttonReset.addEventListener('click', resetDatabase, false);

  buttonStop.disabled = true;

  async function changeFile(event) {
    listUpdate();
  }

  async function getUploadedBytes(pk) {
    const response = await fetch('status', {
      headers: {
        'X-File-Id': pk,
      },
    });
    if (response.status !== 200) {
      throw new Error(`Can't get uploaded bytes: ${response.statusText}`);
    }
    const text = await response.text();
    return +text;
  }

  async function resetDatabase() {
    const response = await fetch('reset');
    if (response.status !== 200) {
      throw new Error(`Can't get uploaded bytes: ${response.statusText}`);
    }
    const text = await response.text();
    listUploadEl.innerHTML = '';
    inputFilesEl.value = '';
    console.warn(text);
  }

  async function uploadFile(event) {
    event.preventDefault();
    isStop = false;
    buttonUpload.disabled = true;
    buttonStop.disabled = false;
    buttonReset.disabled = true;
    console.warn('uploadFile 1');
    const {files} = inputFilesEl;
    if (!files) return;

    for (let idx = 0; idx < files.length; idx += 1) {
      console.warn('uploadFile 2');
      if (isStop) {
        break;
      }
      const file = files[idx];
      uploader = new Uploader({file, onProgress});

      try {
        const uploaded = await uploader.upload();
        console.warn('uploadFile 3');
        if (uploaded) {
          console.log(file.name, 'success');
        } else {
          console.log(file.name, 'stopped');
        }
      } catch (err) {
        console.log(file.name, 'error');
        console.error(err);
      }
    }
    buttonUpload.disabled = false;
    buttonStop.disabled = true;
    buttonReset.disabled = false;
  }

  function uploadStop() {
    console.error('STOP');
    if (uploader) {
      uploader.stop();
      buttonUpload.disabled = false;
      buttonStop.disabled = true;
      buttonReset.disabled = false;
    }
    isStop = true;
  }

  async function listUpdate() {
    listUploadEl.innerHTML = '';
    const {files} = inputFilesEl;
    console.warn(files);
    for (let i = 0; i < files.length; i += 1) {
      const item = document.createElement('li');
      const fileName = files[i].name;
      const fileSize = files[i].size;
      item.dataset.fileName = fileName.replace(/\.[^/.]+$/, '');
      console.warn(fileName.replace(/\.[^/.]+$/, ''));
      const startByte = await getUploadedBytes(fileName);
      const percent = ((startByte / fileSize) * 100).toFixed(2);
      item.innerHTML = `${fileName}  (${startByte}/${fileSize})  [${percent}%]`;
      listUploadEl.appendChild(item);
    }
  }

  function onProgress(fileId, loaded, total) {
    const fileName = fileId.replace(/\.[^/.]+$/, '');
    const targetEl = listUploadEl.querySelector(`li[data-file-name=${fileName}]`);
    const percent = ((loaded / total) * 100).toFixed(2);
    targetEl.innerHTML = `${fileName}  (${loaded}/${total})  [${percent}%]`;
  }
})(this);
